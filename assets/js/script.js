
$(function() {
	
	
	/*[Contacts box <]*/
	if ($('.contacts-box').length) {
		ymaps.ready(function() {
			$('.contacts-box .map').each(function(index, element) {
				var $this = $(this),
					lat = parseFloat($this.data('lat')),
					lng = parseFloat($this.data('lng')),
					zoom = parseInt($this.data('zoom'));
					
				var map = new ymaps.Map(this, {
					zoom: zoom,
					controls: [],
					center: [lat, lng]
				});
					
				map.controls.add('zoomControl', {
					size:'small',
					position: {top: 10, left: 10}
				});
				
				map.behaviors.disable([
					'scrollZoom'
				]);
				
				map.geoObjects.add(new ymaps.Placemark([lat, lng], {}, {
						iconImageSize: [35, 43],
						iconLayout: 'default#image',
						iconImageOffset: [-18, -43],
						iconImageHref: 'assets/img/elements/site/ico/6-1.svg'
					}
				));
			});
		});
		
		$('.contacts-box nav.menu span').click(function(e) {
			var $this = $(this),
				index = $this.index();
			$this.addClass('active').siblings().removeClass('active');
			$('.contacts-box .item').eq(index).addClass('active').siblings().removeClass('active');
		});
	};
	/*[Contacts box >]*/
	
	
	$(window).on('resize', function(e) {
		$('.gallery').css({'width': $(window).innerWidth() - 40});
		
		if ($(window).innerWidth() < 1200) {
			$('body').addClass('_mobile');
			$('.news-box .head h1').after($('.news-box .materials'));
		} else {
			$('.news-box .group > .item:eq(1)').after($('.news-box .materials'));
		};
	}).trigger('resize');
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	$(window).on('resize', function(e) {
		if ($(window).innerWidth() >= 1200) {
			$('.product-box .contain').prependTo($('.product-box .top .cell._2'));
			$('.product-box hgroup').prependTo($('.product-box .top .cell._2'));
		} else {
			$('.product-box .contain').prependTo($('.product-box .top'));
			$('.product-box hgroup').prependTo($('.product-box .top'));
		};
	}).trigger('resize');
	
	
	
	
	$(window).on('resize', function(e) {
		if ($(window).innerWidth() >= 1200) {
			if (!$('.search-box .column').length) {
				$('.search-box .group').append(
					'<div class="column _1"></div>'+
					'<div class="column _2"></div>'+
					'<div class="column _3"></div>'
				);
				
				$('.search-box .group > .item:nth-child(3n)').appendTo($('.search-box .column._3'));
				$('.search-box .group > .item:nth-child(2n)').appendTo($('.search-box .column._2'));
				$('.search-box .group > .item:nth-child(1n)').appendTo($('.search-box .column._1'));
				
				var column_1_height = $('.search-box .column._1').outerHeight(),
					column_2_height = $('.search-box .column._2').outerHeight(),
					column_3_height = $('.search-box .column._3').outerHeight();
				
				if (column_1_height < column_2_height || column_1_height < column_3_height) {
					$('.search-box .column._2 .item:last-child').appendTo($('.search-box .column._1'))
					$('.search-box .column._3 .item:last-child').appendTo($('.search-box .column._2'))
				};
			};
		} else {
			if ($('.search-box .column').length) {
				$('.search-box .item').each(function(index, element) {
					var n = 1;
					if (n === 1) {n = 2; $('.search-box .column._1 .item:first').appendTo($('.search-box .group'))};
					if (n === 2) {n = 3; $('.search-box .column._2 .item:first').appendTo($('.search-box .group'))};
					if (n === 3) {n = 1; $('.search-box .column._3 .item:first').appendTo($('.search-box .group'))};
				});	$('.search-box .column').remove();
			};
		};
	}).trigger('resize');
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	$('select').selectOrDie({
		placeholderOption: true
	});
	$('header.main .controlls > .trigger').on('click', function(e) {
		$(this).parent().toggleClass('open');
		return false;
	});
	
	
	
	
	
	
	
	/*[Search <]*/
	var searchHide = function() {
		$('header.main .search').removeClass('open');
		navMainRestoreSubmenu();
	};
	
	$(document).on('click', function(e) {
		if (!$('body').hasClass('_search') && 
			$('header.main .search').hasClass('open') && 
			$(e.target).closest('header.main .search').length == 0) {
			searchHide();
		};
	});
	
	$('header.main .search .trigger').on('click', function(e) {
		var $this = $(this).parent();
		if (!$this.hasClass('open')) {
			navLangHide();
			$this.addClass('open');
			if (!$('body').hasClass('_mobile')) {
				TweenMax.to('nav.main', .2, {
					paddingBottom: 0,
					ease: Linear.easeNone
				});
			};
		} else {searchHide()};
		return false;
	});
	/*[Search >]*/
	
	/*[Gallery <]*/
	var setGallery = function() {
		var gallerySlidesCounter = function(slick) {
			var current_slide = slick.currentTarget.slick.currentSlide,
				slides_count = slick.currentTarget.slick.slideCount;
			$('.gallery .counter').html((current_slide + 1)+' / '+slides_count);
		};
		
		$('.gallery .slides .wrap').slick({
			fade: true,
			speed: 200,
			swipe: false,
			autoplay: false,
			slidesToShow: 1,
			draggable: false,
			slidesToScroll: 1,
			adaptiveHeight: true,
			cssEase: 'ease-in-out',
			appendArrows: '.gallery .slides',
			asNavFor: '.gallery .thumbs .wrap',
			prevArrow: 
				'<button class="prev" type="button">'+
					'<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="23px" height="15px" viewBox="0 0 23 15" enable-background="new 0 0 23 15" xml:space="preserve">'+
						'<path fill="#009146" d="M9.25,0h-3c0,2.953-2.944,6.003-6,6.003v3C4.953,9.003,9.25,4.547,9.25,0z"/><path fill="#009146" d="M23,8.998H0v-3h23V8.998z"/><path fill="#009146" d="M9.25,15h-3c0-2.953-2.944-6.003-6-6.003v-3C4.953,5.997,9.25,10.453,9.25,15z"/>'+
					'</svg>'+
				'</button>',
			nextArrow: 
				'<button class="next" type="button">'+
					'<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="23px" height="15px" viewBox="0 0 23 15" enable-background="new 0 0 23 15" xml:space="preserve">'+
						'<path fill="#009146" d="M22.75,9.003v-3c-3.057,0-6-3.05-6-6.003h-3C13.75,4.547,18.047,9.003,22.75,9.003z"/><path fill="#009146" d="M0,5.998h23v3H0V5.998z"/><path fill="#009146" d="M22.75,5.997v3c-3.057,0-6,3.05-6,6.003h-3C13.75,10.453,18.047,5.997,22.75,5.997z"/>'+
					'</svg>'+
				'</button>'
		}).on('init', function(slick) {
			gallerySlidesCounter(slick);
		}).on('setPosition', function(slick) {
			gallerySlidesCounter(slick);
		}).on('beforeChange', function(slick, currentSlide, nextSlide) {
			gallerySlidesCounter(slick);
		});
		
		$('.gallery .thumbs .wrap').slick({
			swipe: false,
			vertical: true,
			slidesToShow: 6,
			draggable: false,
			slidesToScroll: 1,
			focusOnSelect: true,
			adaptiveHeight: true,
			appendArrows: '.gallery .thumbs',
			asNavFor: '.gallery .slides .wrap',
			prevArrow: '<button class="prev" type="button"></button>',
			nextArrow: '<button class="next" type="button"></button>'
		});
		
		$('.gallery .thumbs .wrap').slick('slickGoTo', 0);
	}; setGallery();
	
	var setImageGallery = function(data) {
		if ($('.gallery .slides .wrap').hasClass('slick-initialized')) {
			$('.gallery .slides .wrap').slick('unslick').find('.slide').remove();
			$('.gallery .thumbs .wrap').slick('unslick').find('.slide').remove();
		};
		
		for (key in data.slides) {
			var title = data.title,
				slide_image = data.slides[key].slide,
				thumb_image = data.slides[key].thumb,
				slide = 
					'<div class="slide">'+
						'<div class="item">'+
							'<div class="image">'+
								'<div class="box">'+
									'<img src="'+slide_image+'" alt="">'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>',
				thumb = 
					'<div class="slide">'+
						'<div class="item">'+
							'<div class="box">'+
								'<img src="'+thumb_image+'" alt="">'+
							'</div>'+
						'</div>'+
					'</div>';
			
			$('.gallery .title').html(title);
			$('.gallery .slides .wrap').append(slide);
			$('.gallery .thumbs .wrap').append(thumb);
		}; setGallery();
	};
	
	$('.gallery-box .item, .product-item .materials a').on('click', function(e) {
		var id = $(this).data('id');
		if (id !== '') {
			$.ajax({
				type: 'post',
				data: {id: id},
				url: '#get-image',
				beforeSend: function() {
					TweenMax.to('#preloader', .6, {opacity: 1, display: 'table'});
				},
				success: function(data) {
					data = {
						title: 'гранат геокчай',
						slides: [
							{
								slide: 'assets/img/images/group-7/slides/1.jpg',
								thumb: 'assets/img/images/group-7/thumbs/1.jpg'
							},{
								title: 'гранат геокчай',
								slide: 'assets/img/images/group-7/slides/2.jpg',
								thumb: 'assets/img/images/group-7/thumbs/2.jpg'
							},{
								title: 'гранат геокчай',
								slide: 'assets/img/images/group-7/slides/1.jpg',
								thumb: 'assets/img/images/group-7/thumbs/3.jpg'
							},{
								title: 'гранат геокчай',
								slide: 'assets/img/images/group-7/slides/2.jpg',
								thumb: 'assets/img/images/group-7/thumbs/4.jpg'
							},{
								title: 'гранат геокчай',
								slide: 'assets/img/images/group-7/slides/1.jpg',
								thumb: 'assets/img/images/group-7/thumbs/5.jpg'
							},{
								title: 'гранат геокчай',
								slide: 'assets/img/images/group-7/slides/2.jpg',
								thumb: 'assets/img/images/group-7/thumbs/6.jpg'
							},{
								title: 'гранат геокчай',
								slide: 'assets/img/images/group-7/slides/1.jpg',
								thumb: 'assets/img/images/group-7/thumbs/1.jpg'
							}
						]
					};
					setImageGallery(data);
					TweenMax.to('#preloader', .6, {opacity: 0, display: 'none'});
					$('.window[data-name="gallery"]').arcticmodal();
					$(window).trigger('resize');
				},
				error: function() {
					TweenMax.to('#preloader', .6, {opacity: 0, display: 'none'});
				}
			}); return false;
		};
	});
	/*[Gallery >]*/
	
	/*[Nav lang <]*/
	var navLangHide = function() {
		$('nav.lang').removeClass('open');
		navMainRestoreSubmenu();
	};
	
	$(document).on('click', function(e) {
		if (!$('body').hasClass('_mobile') && 
			$('nav.lang').hasClass('open') && 
			$(e.target).closest('nav.lang').length == 0) {
			navLangHide();
		};
	});
	
	$('nav.lang .trigger').on('click', function(e) {
		if (!$('body').hasClass('_mobile')) {
			var $this = $(this).parent();
			if (!$this.hasClass('open')) {
				searchHide();
				$this.addClass('open');
				TweenMax.to('nav.main', .2, {
					paddingBottom: 0,
					ease: Linear.easeNone
				});
			} else {navLangHide()};
		}; return false;
	});
	/*[Nav lang >]*/
	
	/*[Nav main <]*/
	var navMainRestoreSubmenu = function() {
		if (!$('nav.lang').hasClass('open') &&
			!$('header.main .search').hasClass('open')) {
			var open_submenu = $('nav.main > ul > li.open > .submenu ul');
			if (open_submenu.length) {
				var open_submenu_height = open_submenu.outerHeight();
				TweenMax.to('nav.main', .2, {
					ease: Linear.easeNone,
					paddingBottom: open_submenu_height
				});
			};
		};
	};
	
	$(window).on('resize', function(e) {
		if ($(window).innerWidth() < 1200) {
			$('nav.main').removeAttr('style');
			$('nav.main > ul > li > .link').on('click', function(e) {
				$(this).parent().toggleClass('open').siblings().removeClass('open');
				return false;
			});
		} else {
			if ($('nav.main > ul > li.open').length) {
				var open_submenu = $('nav.main > ul > li.open > .submenu ul'),
					open_submenu_height = open_submenu.outerHeight();
				$('nav.main').css({'padding-bottom': open_submenu_height});
			};
				
			$('nav.main > ul > li').hover(function(e) {
				if (!$('nav.lang').hasClass('open') &&
					!$('header.main .search').hasClass('open')) {
					var $this = $(this),
						submenu = $this.find('.submenu > ul');
					if (!submenu.length) {
						TweenMax.to('nav.main', .2, {'padding-bottom': 0, ease: Linear.easeNone});
					} else {
						var submenu_height = submenu.outerHeight();
						TweenMax.to('nav.main', .2, {'padding-bottom': submenu_height, ease: Linear.easeNone});
					};
				};
			}, function(e) {
				if (!$('nav.lang').hasClass('open') &&
					!$('header.main .search').hasClass('open')) {
					if (!$('nav.main > ul > li.open').length) {
						TweenMax.to('nav.main', .2, {'padding-bottom': 0, ease: Linear.easeNone});
					} else {
						TweenMax.to('nav.main', .2, {'padding-bottom': open_submenu_height, ease: Linear.easeNone});
					};
				};
			});
		};
	}).trigger('resize');
	
	$(document).on('click', function(e) {
		if ($('body').hasClass('_mobile') && 
			$(e.target).closest('header.main .controlls').length == 0) {
			$('nav.main > ul > li, header.main .controlls').removeClass('open');
		};
	});
	/*[Nav main >]*/
	
	/*[Photo slider <]*/
	if ($('.photo-slider').length) {
		var setPhotoSliderMobile = function() {
			if ($('.photo-slider .slides').hasClass('slick-initialized')) {
				$('.photo-slider .slides').slick('unslick');
			};
			$('.photo-slider .slides').slick({
				autoplay: false,
				slidesToShow: 1,
				slidesToScroll: 1,
				adaptiveHeight: true,
				appendArrows: '.photo-slider .controlls',
				prevArrow: 
					'<button class="prev" type="button">'+
						'<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="23px" height="15px" viewBox="0 0 23 15" enable-background="new 0 0 23 15" xml:space="preserve">'+
							'<path fill="#009146" d="M9.25,0h-3c0,2.953-2.944,6.003-6,6.003v3C4.953,9.003,9.25,4.547,9.25,0z"/><path fill="#009146" d="M23,8.998H0v-3h23V8.998z"/><path fill="#009146" d="M9.25,15h-3c0-2.953-2.944-6.003-6-6.003v-3C4.953,5.997,9.25,10.453,9.25,15z"/>'+
						'</svg>'+
					'</button>',
				nextArrow: 
					'<button class="next" type="button">'+
						'<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="23px" height="15px" viewBox="0 0 23 15" enable-background="new 0 0 23 15" xml:space="preserve">'+
							'<path fill="#009146" d="M22.75,9.003v-3c-3.057,0-6-3.05-6-6.003h-3C13.75,4.547,18.047,9.003,22.75,9.003z"/><path fill="#009146" d="M0,5.998h23v3H0V5.998z"/><path fill="#009146" d="M22.75,5.997v3c-3.057,0-6,3.05-6,6.003h-3C13.75,10.453,18.047,5.997,22.75,5.997z"/>'+
						'</svg>'+
					'</button>'
			});
		};
		
		var setPhotoSliderDesktop = function() {
			var photo_amount = $('.photo-slider .item').length;
			if ($('.photo-slider .slides').hasClass('slick-initialized')) {
				$('.photo-slider .slides').slick('unslick');
			};
			$('.photo-slider .slides').slick({
				rows: 2,
				infinite: false,
				autoplay: false,
				slidesToShow: 3,
				slidesToScroll: 1,
				adaptiveHeight: true,
				appendArrows: '.photo-slider .controlls',
				prevArrow: 
					'<button class="prev" type="button">'+
						'<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="23px" height="15px" viewBox="0 0 23 15" enable-background="new 0 0 23 15" xml:space="preserve">'+
							'<path fill="#009146" d="M9.25,0h-3c0,2.953-2.944,6.003-6,6.003v3C4.953,9.003,9.25,4.547,9.25,0z"/><path fill="#009146" d="M23,8.998H0v-3h23V8.998z"/><path fill="#009146" d="M9.25,15h-3c0-2.953-2.944-6.003-6-6.003v-3C4.953,5.997,9.25,10.453,9.25,15z"/>'+
						'</svg>'+
					'</button>',
				nextArrow: 
					'<div class="amount">'+photo_amount+' фото</div>'+
					'<button class="next" type="button">'+
						'<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="23px" height="15px" viewBox="0 0 23 15" enable-background="new 0 0 23 15" xml:space="preserve">'+
							'<path fill="#009146" d="M22.75,9.003v-3c-3.057,0-6-3.05-6-6.003h-3C13.75,4.547,18.047,9.003,22.75,9.003z"/><path fill="#009146" d="M0,5.998h23v3H0V5.998z"/><path fill="#009146" d="M22.75,5.997v3c-3.057,0-6,3.05-6,6.003h-3C13.75,10.453,18.047,5.997,22.75,5.997z"/>'+
						'</svg>'+
					'</button>'
			});
		};
		
		$(window).on('resize', function(e) {
			if ($(window).innerWidth() >= 1200) {
				setPhotoSliderDesktop();
			} else {
				setPhotoSliderMobile();
			};
		}).trigger('resize');
	};
	/*[Photo slider >]*/
	
	$('a[href^="#"]').on('click', function(e) {
		var $this = $(this),
			target = $($this.attr('href'));
		if (target.length) {
			var y = target.offset().top;
			$('html, body').animate({scrollTop: y}, 'slow');
			return false;
		};
	});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*[=> GROUP <=]*/
	/*[=> GROUP <=]*/
	
	/*[=> SINGLE <=]*/
	/*[=> SINGLE <=]*/
	
	/*[=> VALIDATION <=]*/
	if ($.validator) {
		
		/*[Settings <]*/
		$.validator.setDefaults({
			highlight: function(element, errorClass, validClass) {
				if (element.type === "radio") {
					this.findByName(element.name).addClass(errorClass).removeClass(validClass);
					this.findByName(element.name).parents('.radio').addClass(errorClass).removeClass(validClass);
				} else {
					$(element).addClass(errorClass).removeClass(validClass);
					$(element).parents('.checkbox, .sod_select').addClass(errorClass).removeClass(validClass);
				};
			},
			unhighlight: function(element, errorClass, validClass) {
				if (element.type === "radio") {
					this.findByName(element.name).removeClass(errorClass).addClass(validClass);
					this.findByName(element.name).parents('.radio').removeClass(errorClass).addClass(validClass);
				} else {
					$(element).removeClass(errorClass).addClass(validClass);
					$(element).parents('.checkbox, .sod_select').removeClass(errorClass).addClass(validClass);
				};
			}
		});
		
		$.validator.addClassRules('_date', {date: true});
		$.validator.addClassRules('_email', {email: true});
		$.validator.addClassRules('_phone', {phone: true});
		$.validator.addClassRules('_digits', {digits: true});
		$.validator.addClassRules('_letters', {letters_only: true});
		$.validator.addClassRules('_letters_digits', {letters_digits: true});
		/*[Settings >]*/
		
		/*[Functions <]*/
		var formReset = function(form) {
			$(form).validate().resetForm();
			$(form).find('input[type="text"], textarea').val('');
			$(form).find('.error, .valid').removeClass('error valid');
			$(form).find('select option:first').prop('selected', true);
			$(form).find('select').selectOrDie('update');
		};
		
		$('button:reset').on('click', function(e) {
			var form = $(this).parents('form'); formReset(form);
		});
		/*[Functions >]*/
		
		/*[Feedback form <]*/
		if ($('#feedback-form').length) {
			$('#feedback-form').validate({
				submitHandler: function() {
					$.ajax({
						type: 'post',
						url: $('#feedback-form').attr('action') || '#',
						data: $('#feedback-form').serialize(),
						beforeSend: function() {
							TweenMax.to('#preloader', .6, {opacity: 1, display: 'table'});
						},
						success: function() {
							formReset('#feedback-form');
							feedbackFormShowMessage('send');
							TweenMax.to('#preloader', .6, {opacity: 0, display: 'none'});
						},
						error: function() {
							TweenMax.to('#preloader', .6, {opacity: 0, display: 'none'});
						}
					});
				},
				errorPlacement: function(error, element) {
					return false;
				}
			});
			
			var feedbackFormHideMessage = function() {
				$('.feedback-form .messages, .feedback-form .messages .item').removeClass('_show');
			};
			
			var feedbackFormShowMessage = function(type) {
				$('.feedback-form .messages, .feedback-form .messages .item._'+type).addClass('_show');
			};
			
			$('.feedback-form .messages .button').on('click', function(e) {
				formReset('#feedback-form');
				feedbackFormHideMessage();
				return false;
			});
		};
		/*[Feedback form >]*/
		
	};
	/*[=> VALIDATION <=]*/
	
	/*[===FORM >]*/
	
});

$(window).load(function() {
	TweenMax.to('#preloader', .6, {opacity: 0, display: 'none'});
});


